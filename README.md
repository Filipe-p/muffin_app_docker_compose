# Muffin Docker Compose APP :doughnut:

This will be a app to showcase docker compose. We'll build it with a php html front page and another container with a python API simulating a DB.

We'll use this class to cover:

- Dev Environments
- Testing & CD environments
- Git and github branching
- Git branching practices and git etiquet
- Docker and docker compose

Will also touch on:

- Python
- API
- Webapps
- Seperation of concerns

Extra objectives:

- add CI
- add CD

### Plan to complete

1. Create simple API with python. Put it in a container. :check:
2. Create simple PHP app to parse/consume JSON sent from api.
3. Build our docker compese